#https://dev.mysql.com/doc/refman/8.0/en/load-data.html

#tab is default for terminated by

load data infile '/home/student/Desktop/dblab2019/week14/movie_stars.csv'
into table movie_stars fields terminated by ',';
#error

show variables like 'secure_file_priv';
#move csv files into this path
#and change their path

load data infile '.../movie_stars.csv'
into table movie_stars fields terminated by ',';
#error

#first load parents entities
load data infile '.../movies.csv'
into table movies fields terminated by ',' enclosed by '"';

load data infile '.../countries.csv'
into table countries fields terminated by ',';

load data infile '.../stars.csv'
into table stars fields terminated by ',' enclosed by '"';

load data infile '.../movie_stars.csv'
into table movie_stars fields terminated by ',';

load data infile '.../directors.csv'
into table directors fields terminated by ',';

load data infile '.../movie_directors.csv'
into table movie_directors fields terminated by ',';

load data infile '.../producer_directors.csv'
into table producer_directors fields terminated by ',';

load data infile '.../genres.csv'
into table genres fields terminated by ',';

load data infile '.../languages.csv'
into table languages fields terminated by ',';
