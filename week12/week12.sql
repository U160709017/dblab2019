#https://www.w3resource.com/mysql/mysql-procedure.php

call selectAllCustomers;

call getCustomersByCity("Barcelona");


select * from Employees;

set @maxSalary = 0;					#it is unnecessary
call highestSalary(@maxSalary);		#we can write it directly
select @maxSalary;
#select * from Employees where Salary = @maxSalary;

set @m_count = 0;
call countGender(@m_count, 'M');
set @f_count = 0;
call countGender(@f_count, 'F');

select @m_count, @f_count;