#1)What is the sum of county FIBS' in each county?
select county_name, sum(county_FIBS)
from counties
group by county_name;

#2)How many hospitals that have zip code 78503?
select count(*)
from hospitals join locations on hospitals.address = locations.address
where zip = "78503";

#3)What is the average of total population in hospitals that have validation method is "IMAGERY"?
select avg(specifications.total_population)
from hospitals join specifications on hospitals.hospital_id = specifications.hospital_id
where hospitals.hospital_id in (
select hospitals.hospital_id
from hospitals join validations on hospitals.hospital_id = validations.hospital_id
where validation_method = "IMAGERY");

#4)How many hospitals do have helipad?
select count(specifications.helipad)
from specifications
where helipad = "Y";

#5)Which children hospital does have maximum population?
select hospitals.hospital_name
from hospitals join specifications on hospitals.hospital_id = specifications.hospital_id
where hospitals.hospital_type = "CHILDREN"
order by specifications.total_population desc
limit 1;

#6)How many hospitals in Jack have NAICS code 622310?
select count(*)
from hospitals join NAICS on hospitals.hospital_id = NAICS.hospital_id
where NAICS_code = "622310" and hospitals.address in(
select hospitals.address
from hospitals join locations on hospitals.address = locations.address
where locations.address in (
select locations.address
from locations join counties on locations.address = counties.address
where county_name = "Jack"));

#7)What is the minumum county FIBS in OH state?
select counties.county_FIBS
from counties join locations on counties.address = locations.address
where locations.address in (
select locations.address
from locations join states on locations.address = states.address
where states.state_name = "OH" )
order by counties.county_FIBS asc
limit 1;

#8)What is the average of available beds in hospitals that have status open?
select avg(available_beds)
from hospitals join specifications on hospitals.hospital_id = specifications.hospital_id
where hospital_status = "OPEN";

#9)How many hospitals have helipad in Winslow?
select count(*)
from hospitals join locations on hospitals.address = locations.address
where locations.address in (
select locations.address
from locations join cities on locations.address = cities.address
where cities.city_name = "Winslow" );

#10)What is the minumum latitude in PRI?
select min(latitude)
from locations join countries on locations.address = countries.address
where country_name = "PRI";

#11)How many hospitals do each type have?
select hospital_type, count(hospital_id)
from hospitals
group by hospital_type;