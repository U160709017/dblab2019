-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema hospitals
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema hospitals
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `hospitals` ;
USE `hospitals` ;

-- -----------------------------------------------------
-- Table `hospitals`.`locations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`locations` (
  `address` VARCHAR(100) NOT NULL,
  `alternate_name` VARCHAR(45) NULL,
  `zip` INT(5) NOT NULL,
  `zip4` INT(4) NULL,
  `latitude` INT NOT NULL,
  `longitude` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`address`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hospitals`.`hospitals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`hospitals` (
  `hospital_id` INT NOT NULL,
  `hospital_name` VARCHAR(45) NOT NULL,
  `address` VARCHAR(100) NOT NULL,
  `hospital_telephone` VARCHAR(14) NULL,
  `hospital_status` VARCHAR(45) NOT NULL,
  `hospital_type` VARCHAR(45) NOT NULL,
  `hospital_owner` VARCHAR(45) NULL,
  PRIMARY KEY (`hospital_id`),
  INDEX `fk_hospitals_locations1_idx` (`address` ASC),
  CONSTRAINT `fk_hospitals_locations1`
    FOREIGN KEY (`address`)
    REFERENCES `hospitals`.`locations` (`address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hospitals`.`states`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`states` (
  `address` VARCHAR(100) NOT NULL,
  `state_name` VARCHAR(2) NOT NULL,
  `state_id` VARCHAR(9) NULL,
  `state_FIBS` VARCHAR(6) NOT NULL,
  PRIMARY KEY (`address`),
  CONSTRAINT `fk_states_location1`
    FOREIGN KEY (`address`)
    REFERENCES `hospitals`.`locations` (`address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hospitals`.`counties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`counties` (
  `address` VARCHAR(100) NOT NULL,
  `county_name` VARCHAR(45) NOT NULL,
  `county_FIBS` INT(5) NULL,
  PRIMARY KEY (`address`),
  CONSTRAINT `fk_counties_location1`
    FOREIGN KEY (`address`)
    REFERENCES `hospitals`.`locations` (`address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hospitals`.`countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`countries` (
  `address` VARCHAR(100) NOT NULL,
  `country_name` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`address`),
  CONSTRAINT `fk_countries_location1`
    FOREIGN KEY (`address`)
    REFERENCES `hospitals`.`locations` (`address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hospitals`.`cities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`cities` (
  `address` VARCHAR(100) NOT NULL,
  `city_name` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`address`),
  CONSTRAINT `fk_cities_location1`
    FOREIGN KEY (`address`)
    REFERENCES `hospitals`.`locations` (`address`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hospitals`.`sources`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`sources` (
  `hospital_id` INT NOT NULL,
  `source_name` VARCHAR(45) NOT NULL,
  `source_date` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`hospital_id`),
  CONSTRAINT `fk_Sources_hospitals1`
    FOREIGN KEY (`hospital_id`)
    REFERENCES `hospitals`.`hospitals` (`hospital_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hospitals`.`NAICS`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`NAICS` (
  `hospital_id` INT NOT NULL,
  `NAICS_code` VARCHAR(24) NOT NULL,
  `NAICS_description` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`hospital_id`),
  CONSTRAINT `fk_NAICS_hospitals1`
    FOREIGN KEY (`hospital_id`)
    REFERENCES `hospitals`.`hospitals` (`hospital_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hospitals`.`websites`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`websites` (
  `hospital_id` INT NOT NULL,
  `website_name` VARCHAR(100) NULL,
  PRIMARY KEY (`hospital_id`),
  CONSTRAINT `fk_website_hospitals`
    FOREIGN KEY (`hospital_id`)
    REFERENCES `hospitals`.`hospitals` (`hospital_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hospitals`.`specifications`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`specifications` (
  `hospital_id` INT NOT NULL,
  `total_population` INT NULL,
  `available_beds` INT NULL,
  `trauma_level` VARCHAR(45) NULL,
  `helipad` VARCHAR(1) NULL,
  PRIMARY KEY (`hospital_id`),
  CONSTRAINT `fk_trauma_hospitals1`
    FOREIGN KEY (`hospital_id`)
    REFERENCES `hospitals`.`hospitals` (`hospital_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `hospitals`.`validation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `hospitals`.`validation` (
  `hospital_id` INT NOT NULL,
  `validation_date` VARCHAR(45) NOT NULL,
  `validation_method` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`hospital_id`),
  CONSTRAINT `fk_validations_hospitals1`
    FOREIGN KEY (`hospital_id`)
    REFERENCES `hospitals`.`hospitals` (`hospital_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
